function createInputs() {

    const $sizeInput = $('<input/>', {
                        class:'size-input',
                        id:'sizeInput',
                        placeholder:'Введите диаметр круга'
    }).appendTo('body'); 

    const $colorInput = $('<input/>', {
                        class:'color-input',
                        id:'colorInput',
                        placeholder:'Введите цвет круга'
    }).appendTo('body'); 

    const $drawCirlce = $('<button/>', {
                        class:'draw-button',
                        id:'drawCircleBtn',
                        text:'Рисовать'
    }).appendTo('body');

$htmlButton.remove();


function circleDrawing() {

    const $sizeInputValue = $('#sizeInput')
    const $diameterValue = $sizeInputValue.val();

    const $colorInputValue = $('#colorInput');
    const $colorValue = $colorInputValue.val();

    let $circleDiv = $('<div/>',{
                        class:'circle-div',
    }).appendTo('body');
    $circleDiv.css({'backgroundColor' : `${$colorValue}`, 
                    'width':`${$diameterValue}px`,
                    'height':`${$diameterValue}px`
                });
    
};
const $drawCircleBtn = $('#drawCircleBtn');
$drawCircleBtn.click(circleDrawing);

};
const $htmlButton = $('#draw');
$htmlButton.click(createInputs);

